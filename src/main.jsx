import React from 'react'
import ReactDOM from 'react-dom/client'
// import App from './App'
import './index.css'
import { PracticaApp } from './PracticaApp'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <PracticaApp />
  </React.StrictMode>,
)
