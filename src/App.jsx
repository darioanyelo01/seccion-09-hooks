import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";

function App() {
  const [ state , setCounter] = useState({
    counter1: 10,
    counter2: 20,
    counter3: 30,
  });

  const {counter1,counter2,counter3} = state
  // const handleOnClick = () => {
  //   setCounter(counter1 + 1);
  // }

  return (
    <div className="App">
      <div>
        <h1>Hola seguimos con React+Jascrip</h1>
        <h2>Sigue bien</h2>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <hr />
      <h1>Esta es la Sección-09-hooks</h1>
      <div className="card">
        {/* <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        */}
        <div>
          <button 
          type="button" 
          className="btn btn-danger"
          
          >
            Danger
          </button>
          <button type="button" className="btn btn-warning">
            Warning
          </button>
        </div>
      </div>
      <hr />
      <p className="read-the-docs">
        Reforzar los conocimientos con los hooks
      </p>
      <hr />

        <h2>Counter1 : {counter1} </h2>
        <h2>Counter2 : {counter2} </h2>
        <h2>Counter3 : {counter3} </h2>



      <div>
        <button 
        type="button" 
        className="btn btn-danger"
        onClick={() => setCounter( {
          ...state,
          counter1: counter1 + 1,
        })}
        >Es un contador de número + </button>
      </div>
      
    </div>
  );
}

export default App;
